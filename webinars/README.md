# Webinars

This folder hosts supporting materials for the webinars developed by Flywheel.
 
A list of previous and upcoming webinars can be found on our documentation page 
[here](https://docs.flywheel.io/hc/en-us/articles/360044852353-Upcoming-webinars).

## Table of contents

* Finding Your Stuff in Flywheel with the Flywheel Python SDK - May 12th, 2020 -
[notebook](https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/blob/master/webinars/finding_things_in_fw/finding_things_in_fw_notebook.ipynb) & [video](https://www.youtube.com/watch?time_continue=166&v=qji1uLmu7iU&feature=emb_logo)
* Uploading and Editing Metadata with Flywheel SDK - July 23rd, 2020 -
[notebook](https://gitlab.com/flywheel-io/public/flywheel-tutorials/-/blob/master/webinars/upload_data_modify_metadata_w_fw/upload-data-modify-metadata-notebook.ipynb) & video - in post-production
